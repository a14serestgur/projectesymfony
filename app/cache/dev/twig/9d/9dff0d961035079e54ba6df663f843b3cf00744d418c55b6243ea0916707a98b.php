<?php

/* espais/content.html.twig */
class __TwigTemplate_41ad2ce76c7a3bc404e4d76717c532b7ce4e151410da78eb79b898b81f2a1972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("::base.html.twig", "espais/content.html.twig", 2);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e393d5b3982c4d31456094dec13dd5145111a6e05248e6247122847421e14d29 = $this->env->getExtension("native_profiler");
        $__internal_e393d5b3982c4d31456094dec13dd5145111a6e05248e6247122847421e14d29->enter($__internal_e393d5b3982c4d31456094dec13dd5145111a6e05248e6247122847421e14d29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "espais/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e393d5b3982c4d31456094dec13dd5145111a6e05248e6247122847421e14d29->leave($__internal_e393d5b3982c4d31456094dec13dd5145111a6e05248e6247122847421e14d29_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2d51bc380b4229572a95758fc6d5fa4b2947c64c750e5c5b975fe8ec80370c6b = $this->env->getExtension("native_profiler");
        $__internal_2d51bc380b4229572a95758fc6d5fa4b2947c64c750e5c5b975fe8ec80370c6b->enter($__internal_2d51bc380b4229572a95758fc6d5fa4b2947c64c750e5c5b975fe8ec80370c6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "        ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        
        $__internal_2d51bc380b4229572a95758fc6d5fa4b2947c64c750e5c5b975fe8ec80370c6b->leave($__internal_2d51bc380b4229572a95758fc6d5fa4b2947c64c750e5c5b975fe8ec80370c6b_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_deee75699de02c031089352f9f3562a3d84378cc0036790a2c916516e43c84e5 = $this->env->getExtension("native_profiler");
        $__internal_deee75699de02c031089352f9f3562a3d84378cc0036790a2c916516e43c84e5->enter($__internal_deee75699de02c031089352f9f3562a3d84378cc0036790a2c916516e43c84e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "        ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
    <div class=\"content\">
        <div id=\"titol\">
            <h3>Llista d'espais</h3>
        </div>
        <div id=\"dades\">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Localitzacio</th>
                    <th>Places Maximes</th>
                </tr>
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["espais"]) ? $context["espais"] : $this->getContext($context, "espais")));
        foreach ($context['_seq'] as $context["_key"] => $context["espai"]) {
            // line 21
            echo "                <tr>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["espai"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["espai"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["espai"], "localitzacio", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["espai"], "placesMaximes", array()), "html", null, true);
            echo "</td>
                </tr>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['espai'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </table>
";
        
        $__internal_deee75699de02c031089352f9f3562a3d84378cc0036790a2c916516e43c84e5->leave($__internal_deee75699de02c031089352f9f3562a3d84378cc0036790a2c916516e43c84e5_prof);

    }

    public function getTemplateName()
    {
        return "espais/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  93 => 25,  89 => 24,  85 => 23,  81 => 22,  78 => 21,  74 => 20,  57 => 7,  51 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/espais/content.html.twig #}*/
/* {% extends '::base.html.twig' %}*/
/*     {% block head %}*/
/*         {{ parent() }}*/
/*     {% endblock %}*/
/*     {% block body %}*/
/*         {{ parent() }}*/
/*     <div class="content">*/
/*         <div id="titol">*/
/*             <h3>Llista d'espais</h3>*/
/*         </div>*/
/*         <div id="dades">*/
/*             <table>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Nom</th>*/
/*                     <th>Localitzacio</th>*/
/*                     <th>Places Maximes</th>*/
/*                 </tr>*/
/*                 {% for espai in espais %}*/
/*                 <tr>*/
/*                     <td>{{ espai.id }}</td>*/
/*                     <td>{{ espai.nom }}</td>*/
/*                     <td>{{ espai.localitzacio }}</td>*/
/*                     <td>{{ espai.placesMaximes }}</td>*/
/*                 </tr>*/
/*             </div>*/
/*         </div>*/
/*         {% endfor %}*/
/*     </table>*/
/* {% endblock %}*/
/* */
