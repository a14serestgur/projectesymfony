<?php

/* entrades/content.html.twig */
class __TwigTemplate_7555a841776c0625d0cab8c569e02960f981804ed9398e67a5dd95b4c392c4d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "entrades/content.html.twig", 2);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15356d14beaa56752ac6abaa9249577d3abb83a4586489a259b891fddf1c0ce8 = $this->env->getExtension("native_profiler");
        $__internal_15356d14beaa56752ac6abaa9249577d3abb83a4586489a259b891fddf1c0ce8->enter($__internal_15356d14beaa56752ac6abaa9249577d3abb83a4586489a259b891fddf1c0ce8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "entrades/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15356d14beaa56752ac6abaa9249577d3abb83a4586489a259b891fddf1c0ce8->leave($__internal_15356d14beaa56752ac6abaa9249577d3abb83a4586489a259b891fddf1c0ce8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b9fd207e33c57b862933fff880c63968a4994d3e295df95f0e205554d5119407 = $this->env->getExtension("native_profiler");
        $__internal_b9fd207e33c57b862933fff880c63968a4994d3e295df95f0e205554d5119407->enter($__internal_b9fd207e33c57b862933fff880c63968a4994d3e295df95f0e205554d5119407_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "        ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        
        $__internal_b9fd207e33c57b862933fff880c63968a4994d3e295df95f0e205554d5119407->leave($__internal_b9fd207e33c57b862933fff880c63968a4994d3e295df95f0e205554d5119407_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_fd74a1ba1fb2776aac0b578adf4907aeaf2a3979b24496e8b60089d9e68c4bb1 = $this->env->getExtension("native_profiler");
        $__internal_fd74a1ba1fb2776aac0b578adf4907aeaf2a3979b24496e8b60089d9e68c4bb1->enter($__internal_fd74a1ba1fb2776aac0b578adf4907aeaf2a3979b24496e8b60089d9e68c4bb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "        ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
     <div class=\"content\">
        <div id=\"titol\">
            <h3>Llista d'entrades</h3>
        </div>
        <div id=\"dades\">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Concert</th>
                    <th>Preu</th>
                </tr>
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entrades"]) ? $context["entrades"] : $this->getContext($context, "entrades")));
        foreach ($context['_seq'] as $context["_key"] => $context["entrada"]) {
            // line 21
            echo "                <tr>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entrada"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["entrada"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entrada"], "idConcert", array()), "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["entrada"], "preu", array()), "html", null, true);
            echo "€</td>
                </tr>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entrada'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </table>
";
        
        $__internal_fd74a1ba1fb2776aac0b578adf4907aeaf2a3979b24496e8b60089d9e68c4bb1->leave($__internal_fd74a1ba1fb2776aac0b578adf4907aeaf2a3979b24496e8b60089d9e68c4bb1_prof);

    }

    public function getTemplateName()
    {
        return "entrades/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  93 => 25,  89 => 24,  85 => 23,  81 => 22,  78 => 21,  74 => 20,  57 => 7,  51 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/entrades/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block head %}*/
/*         {{ parent() }}*/
/*     {% endblock %}*/
/*     {% block body %}*/
/*         {{ parent() }}*/
/*      <div class="content">*/
/*         <div id="titol">*/
/*             <h3>Llista d'entrades</h3>*/
/*         </div>*/
/*         <div id="dades">*/
/*             <table>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Nom</th>*/
/*                     <th>Concert</th>*/
/*                     <th>Preu</th>*/
/*                 </tr>*/
/*                 {% for entrada in entrades %}*/
/*                 <tr>*/
/*                     <td>{{ entrada.id }}</td>*/
/*                     <td>{{ entrada.nom }}</td>*/
/*                     <td>{{ entrada.idConcert.nom }}</td>*/
/*                     <td>{{ entrada.preu }}€</td>*/
/*                 </tr>*/
/*             </div>*/
/*         </div>*/
/*         {% endfor %}*/
/*     </table>*/
/* {% endblock %}*/
