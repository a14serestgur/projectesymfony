<?php

/* base.html.twig */
class __TwigTemplate_8d0d072954ad16c0d1f730569b031135220123357e4edab4bc06811a5ae32997 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1dabc52b67031316d8ee91983f35fcd8ae40d75e6142fc93bb15131fd3bf6679 = $this->env->getExtension("native_profiler");
        $__internal_1dabc52b67031316d8ee91983f35fcd8ae40d75e6142fc93bb15131fd3bf6679->enter($__internal_1dabc52b67031316d8ee91983f35fcd8ae40d75e6142fc93bb15131fd3bf6679_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 56
        echo "    </body>
</html>
";
        
        $__internal_1dabc52b67031316d8ee91983f35fcd8ae40d75e6142fc93bb15131fd3bf6679->leave($__internal_1dabc52b67031316d8ee91983f35fcd8ae40d75e6142fc93bb15131fd3bf6679_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a260b9b02fff9cc87816286ecb54866f49f552d6a623602e33fab05dbbdd0820 = $this->env->getExtension("native_profiler");
        $__internal_a260b9b02fff9cc87816286ecb54866f49f552d6a623602e33fab05dbbdd0820->enter($__internal_a260b9b02fff9cc87816286ecb54866f49f552d6a623602e33fab05dbbdd0820_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a260b9b02fff9cc87816286ecb54866f49f552d6a623602e33fab05dbbdd0820->leave($__internal_a260b9b02fff9cc87816286ecb54866f49f552d6a623602e33fab05dbbdd0820_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f9fe12c15f72bda1600c381578e0d82a1be1a5ca28b40cb4e29a3848aca154e9 = $this->env->getExtension("native_profiler");
        $__internal_f9fe12c15f72bda1600c381578e0d82a1be1a5ca28b40cb4e29a3848aca154e9->enter($__internal_f9fe12c15f72bda1600c381578e0d82a1be1a5ca28b40cb4e29a3848aca154e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/acmedemo/css/theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        ";
        
        $__internal_f9fe12c15f72bda1600c381578e0d82a1be1a5ca28b40cb4e29a3848aca154e9->leave($__internal_f9fe12c15f72bda1600c381578e0d82a1be1a5ca28b40cb4e29a3848aca154e9_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_7bc639f7c3ac8fcd5eb0eb3856d7cc572b822d7f55bc3a8dda6ead945a87f96e = $this->env->getExtension("native_profiler");
        $__internal_7bc639f7c3ac8fcd5eb0eb3856d7cc572b822d7f55bc3a8dda6ead945a87f96e->enter($__internal_7bc639f7c3ac8fcd5eb0eb3856d7cc572b822d7f55bc3a8dda6ead945a87f96e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "    <div id=\"menu\">
        <ul id=\"nav\">
            <li><a href=\"#\">Grups</a>
                <ul>
                    <li><a href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectGrup"), "html", null, true);
        echo "\">Buscar Grup per Nom</a></li>
                    <li><a href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllGrups"), "html", null, true);
        echo "\">Buscar tots els Grups</a></li>
                    <li><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllGrupsOrderedByName"), "html", null, true);
        echo "\">Buscar Grups Ordenats per Nom</a></li>
                    <li><a href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/insertGrup"), "html", null, true);
        echo "\">Insertar Grup</a></li>
                    <li><a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/deleteGrup"), "html", null, true);
        echo "\">Borrar Grup</a></li>
                    <li><a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/updateGrup"), "html", null, true);
        echo "\">Editar Grup</a></li>
                </ul>
            </li>
            <li><a href=\"#\">Espais</a>
                <ul>
                    <li><a href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectEspai"), "html", null, true);
        echo "\">Buscar Espai per Nom</a></li>
                    <li><a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllEspais"), "html", null, true);
        echo "\">Buscar tots els Espais</a></li>
                    <li><a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllEspaisOrderedByName"), "html", null, true);
        echo "\">Buscar Espais Ordenats per Nom</a></li>
                    <li><a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/insertEspai"), "html", null, true);
        echo "\">Insertar Espai</a></li>
                    <li><a href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/deleteEspai"), "html", null, true);
        echo "\">Eliminar Espai</a></li>
                    <li><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/updateEspai"), "html", null, true);
        echo "\">Editar Espai</a></li>
                </ul>
            </li>
            <li><a href=\"#\">Concerts</a>
                <ul>
                    <li><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectConcert"), "html", null, true);
        echo "\">Buscar Concert per Data</a></li>
                    <li><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllConcerts"), "html", null, true);
        echo "\">Buscar tots els Concerts</a></li>
                    <li><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/insertConcert"), "html", null, true);
        echo "\">Insertar Concert</a></li>
                    <li><a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/deleteConcert"), "html", null, true);
        echo "\">Eliminar Concert</a></li>
                    <li><a href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/updateConcert"), "html", null, true);
        echo "\">Editar Concert</a></li>
                </ul>
            </li>
            <li><a href=\"#\">Entrades</a>
                <ul>
                    <li><a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectEntrada"), "html", null, true);
        echo "\">Buscar Entrada per Nom</a></li>
                    <li><a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/selectAllEntrades"), "html", null, true);
        echo "\">Buscar totes les Entrades</a></li>
                    <li><a href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/insertEntrada"), "html", null, true);
        echo "\">Insertar Entrada</a></li>
                    <li><a href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/deleteEntrada"), "html", null, true);
        echo "\">Eliminar Entrada</a></li>
                    <li><a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("app_dev.php/updateEntrada"), "html", null, true);
        echo "\">Editar Entrada</a></li>
                </ul>
            </li>
        </ul>
    </div>
    ";
        
        $__internal_7bc639f7c3ac8fcd5eb0eb3856d7cc572b822d7f55bc3a8dda6ead945a87f96e->leave($__internal_7bc639f7c3ac8fcd5eb0eb3856d7cc572b822d7f55bc3a8dda6ead945a87f96e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 50,  187 => 49,  183 => 48,  179 => 47,  175 => 46,  167 => 41,  163 => 40,  159 => 39,  155 => 38,  151 => 37,  143 => 32,  139 => 31,  135 => 30,  131 => 29,  127 => 28,  123 => 27,  115 => 22,  111 => 21,  107 => 20,  103 => 19,  99 => 18,  95 => 17,  89 => 13,  83 => 12,  73 => 7,  67 => 6,  55 => 5,  46 => 56,  44 => 12,  37 => 9,  35 => 6,  31 => 5,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*         <link href="{{ asset('bundles/acmedemo/css/theme.css') }}" rel="stylesheet" />*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*     {% block body %}*/
/*     <div id="menu">*/
/*         <ul id="nav">*/
/*             <li><a href="#">Grups</a>*/
/*                 <ul>*/
/*                     <li><a href="{{ asset('app_dev.php/selectGrup') }}">Buscar Grup per Nom</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllGrups') }}">Buscar tots els Grups</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllGrupsOrderedByName') }}">Buscar Grups Ordenats per Nom</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/insertGrup') }}">Insertar Grup</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/deleteGrup') }}">Borrar Grup</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/updateGrup') }}">Editar Grup</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li><a href="#">Espais</a>*/
/*                 <ul>*/
/*                     <li><a href="{{ asset('app_dev.php/selectEspai') }}">Buscar Espai per Nom</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllEspais') }}">Buscar tots els Espais</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllEspaisOrderedByName') }}">Buscar Espais Ordenats per Nom</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/insertEspai') }}">Insertar Espai</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/deleteEspai') }}">Eliminar Espai</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/updateEspai') }}">Editar Espai</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li><a href="#">Concerts</a>*/
/*                 <ul>*/
/*                     <li><a href="{{ asset('app_dev.php/selectConcert') }}">Buscar Concert per Data</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllConcerts') }}">Buscar tots els Concerts</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/insertConcert') }}">Insertar Concert</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/deleteConcert') }}">Eliminar Concert</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/updateConcert') }}">Editar Concert</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*             <li><a href="#">Entrades</a>*/
/*                 <ul>*/
/*                     <li><a href="{{ asset('app_dev.php/selectEntrada') }}">Buscar Entrada per Nom</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/selectAllEntrades') }}">Buscar totes les Entrades</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/insertEntrada') }}">Insertar Entrada</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/deleteEntrada') }}">Eliminar Entrada</a></li>*/
/*                     <li><a href="{{ asset('app_dev.php/updateEntrada') }}">Editar Entrada</a></li>*/
/*                 </ul>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/*     {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
