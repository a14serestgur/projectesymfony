<?php

/* default/missatge.html.twig */
class __TwigTemplate_020bad48873306db44230826181f2adb27732daaea928b14a64737346e9051bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/missatge.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f2e6cc2695743fb3017055f05bacb73ab8382148ae58b6f118f62af26497b71a = $this->env->getExtension("native_profiler");
        $__internal_f2e6cc2695743fb3017055f05bacb73ab8382148ae58b6f118f62af26497b71a->enter($__internal_f2e6cc2695743fb3017055f05bacb73ab8382148ae58b6f118f62af26497b71a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/missatge.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f2e6cc2695743fb3017055f05bacb73ab8382148ae58b6f118f62af26497b71a->leave($__internal_f2e6cc2695743fb3017055f05bacb73ab8382148ae58b6f118f62af26497b71a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_bbe6f4b450abc9e7c70a89198f96f45395a610b773501e8d567bb2bf0f7acf79 = $this->env->getExtension("native_profiler");
        $__internal_bbe6f4b450abc9e7c70a89198f96f45395a610b773501e8d567bb2bf0f7acf79->enter($__internal_bbe6f4b450abc9e7c70a89198f96f45395a610b773501e8d567bb2bf0f7acf79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
<div class=\"content\">
    <div id=\"dades\">
    ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["missatge"]) ? $context["missatge"] : $this->getContext($context, "missatge")), "html", null, true);
        echo "
    </div>
</div>
";
        
        $__internal_bbe6f4b450abc9e7c70a89198f96f45395a610b773501e8d567bb2bf0f7acf79->leave($__internal_bbe6f4b450abc9e7c70a89198f96f45395a610b773501e8d567bb2bf0f7acf79_prof);

    }

    public function getTemplateName()
    {
        return "default/missatge.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 7,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* <div class="content">*/
/*     <div id="dades">*/
/*     {{ missatge }}*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
