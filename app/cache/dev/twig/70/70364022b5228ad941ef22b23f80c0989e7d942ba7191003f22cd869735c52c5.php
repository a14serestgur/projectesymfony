<?php

/* default/form.html.twig */
class __TwigTemplate_3115c95a228e37b4597abaf6d5b86452fb3061378a7bb418d3ab57fb41117754 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/form.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_617ccd12bee083e0ce392db2854000fdc44f949376a6225ac6218ab3a833922d = $this->env->getExtension("native_profiler");
        $__internal_617ccd12bee083e0ce392db2854000fdc44f949376a6225ac6218ab3a833922d->enter($__internal_617ccd12bee083e0ce392db2854000fdc44f949376a6225ac6218ab3a833922d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_617ccd12bee083e0ce392db2854000fdc44f949376a6225ac6218ab3a833922d->leave($__internal_617ccd12bee083e0ce392db2854000fdc44f949376a6225ac6218ab3a833922d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e024f0b251494e47d00806f8ce34dee35c1efbd5fb216df8ef82ae02b922d62b = $this->env->getExtension("native_profiler");
        $__internal_e024f0b251494e47d00806f8ce34dee35c1efbd5fb216df8ef82ae02b922d62b->enter($__internal_e024f0b251494e47d00806f8ce34dee35c1efbd5fb216df8ef82ae02b922d62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
<div class=\"content\">
    <div id=\"titol\"> 
    <h3>";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "</h3>
    </div>
    <div id=\"dades\">
    ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
</div>
";
        
        $__internal_e024f0b251494e47d00806f8ce34dee35c1efbd5fb216df8ef82ae02b922d62b->leave($__internal_e024f0b251494e47d00806f8ce34dee35c1efbd5fb216df8ef82ae02b922d62b_prof);

    }

    public function getTemplateName()
    {
        return "default/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 12,  57 => 11,  53 => 10,  47 => 7,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* <div class="content">*/
/*     <div id="titol"> */
/*     <h3>{{title}}</h3>*/
/*     </div>*/
/*     <div id="dades">*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
