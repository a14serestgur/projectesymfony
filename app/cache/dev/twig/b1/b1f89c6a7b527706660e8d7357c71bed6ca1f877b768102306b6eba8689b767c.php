<?php

/* concerts/content.html.twig */
class __TwigTemplate_16291302b669880d4c1780f3baa5a930a721e79562838768615a6e052142da8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "concerts/content.html.twig", 2);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40a3f5296c7c28307fb99e74d480e2957019c8a0c2eb6e98b0a5eed1d11973de = $this->env->getExtension("native_profiler");
        $__internal_40a3f5296c7c28307fb99e74d480e2957019c8a0c2eb6e98b0a5eed1d11973de->enter($__internal_40a3f5296c7c28307fb99e74d480e2957019c8a0c2eb6e98b0a5eed1d11973de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "concerts/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40a3f5296c7c28307fb99e74d480e2957019c8a0c2eb6e98b0a5eed1d11973de->leave($__internal_40a3f5296c7c28307fb99e74d480e2957019c8a0c2eb6e98b0a5eed1d11973de_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d3ed557c2aaffbd985bb6c723fd4334c55ccae0c812af633e22bfeb866fe2581 = $this->env->getExtension("native_profiler");
        $__internal_d3ed557c2aaffbd985bb6c723fd4334c55ccae0c812af633e22bfeb866fe2581->enter($__internal_d3ed557c2aaffbd985bb6c723fd4334c55ccae0c812af633e22bfeb866fe2581_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "        ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        
        $__internal_d3ed557c2aaffbd985bb6c723fd4334c55ccae0c812af633e22bfeb866fe2581->leave($__internal_d3ed557c2aaffbd985bb6c723fd4334c55ccae0c812af633e22bfeb866fe2581_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_42acf8455b64ec5f8c7a6779b72d29e23afca005b7f5137bc431c0bda398fb6e = $this->env->getExtension("native_profiler");
        $__internal_42acf8455b64ec5f8c7a6779b72d29e23afca005b7f5137bc431c0bda398fb6e->enter($__internal_42acf8455b64ec5f8c7a6779b72d29e23afca005b7f5137bc431c0bda398fb6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "        ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
    <div class=\"content\">
        <div id=\"titol\">
            <h3>Llista Concerts</h3>
        </div>
        <div id=\"dades\">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Grup</th>
                    <th>Data</th>
                    <th>Espai</th>
                </tr>
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["concerts"]) ? $context["concerts"] : $this->getContext($context, "concerts")));
        foreach ($context['_seq'] as $context["_key"] => $context["concert"]) {
            // line 22
            echo "                <tr>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["concert"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["concert"], "idGrup", array()), "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["concert"], "data", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
                    <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["concert"], "idEspai", array()), "nom", array()), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </table>
        </div>
    </div>
";
        
        $__internal_42acf8455b64ec5f8c7a6779b72d29e23afca005b7f5137bc431c0bda398fb6e->leave($__internal_42acf8455b64ec5f8c7a6779b72d29e23afca005b7f5137bc431c0bda398fb6e_prof);

    }

    public function getTemplateName()
    {
        return "concerts/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 30,  98 => 27,  94 => 26,  90 => 25,  86 => 24,  82 => 23,  79 => 22,  75 => 21,  57 => 7,  51 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/concerts/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block head %}*/
/*         {{ parent() }}*/
/*     {% endblock %}*/
/*     {% block body %}*/
/*         {{ parent() }}*/
/*     <div class="content">*/
/*         <div id="titol">*/
/*             <h3>Llista Concerts</h3>*/
/*         </div>*/
/*         <div id="dades">*/
/*             <table>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Nom</th>*/
/*                     <th>Grup</th>*/
/*                     <th>Data</th>*/
/*                     <th>Espai</th>*/
/*                 </tr>*/
/*             {% for concert in concerts %}*/
/*                 <tr>*/
/*                     <td>{{ concert.id }}</td>*/
/*                     <td>{{ concert.nom }}</td>*/
/*                     <td>{{ concert.idGrup.nom }}</td>*/
/*                     <td>{{ concert.data.format('Y-m-d') }}</td>*/
/*                     <td>{{ concert.idEspai.nom }}</td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
