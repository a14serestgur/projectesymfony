<?php

/* grups/content.html.twig */
class __TwigTemplate_9765127865a502ae326d4dcb4e801da7fe8d944b383d2e3f670affa600193113 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "grups/content.html.twig", 2);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f62abdfa1766d9342aa549205990b26c7fc7fe401b76dea0d85b582fbc7e4d24 = $this->env->getExtension("native_profiler");
        $__internal_f62abdfa1766d9342aa549205990b26c7fc7fe401b76dea0d85b582fbc7e4d24->enter($__internal_f62abdfa1766d9342aa549205990b26c7fc7fe401b76dea0d85b582fbc7e4d24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "grups/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f62abdfa1766d9342aa549205990b26c7fc7fe401b76dea0d85b582fbc7e4d24->leave($__internal_f62abdfa1766d9342aa549205990b26c7fc7fe401b76dea0d85b582fbc7e4d24_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c6f260ffb9c781e363dc12bdf0c085ad3356a410c3709d81cf17d88ee80d00cb = $this->env->getExtension("native_profiler");
        $__internal_c6f260ffb9c781e363dc12bdf0c085ad3356a410c3709d81cf17d88ee80d00cb->enter($__internal_c6f260ffb9c781e363dc12bdf0c085ad3356a410c3709d81cf17d88ee80d00cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "        ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        
        $__internal_c6f260ffb9c781e363dc12bdf0c085ad3356a410c3709d81cf17d88ee80d00cb->leave($__internal_c6f260ffb9c781e363dc12bdf0c085ad3356a410c3709d81cf17d88ee80d00cb_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_2533a267de5754d2c83b1ddf472ecfdb2aff4b4f562085cb6064ae377988d405 = $this->env->getExtension("native_profiler");
        $__internal_2533a267de5754d2c83b1ddf472ecfdb2aff4b4f562085cb6064ae377988d405->enter($__internal_2533a267de5754d2c83b1ddf472ecfdb2aff4b4f562085cb6064ae377988d405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "        ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
    <div class=\"content\">
        <div id=\"titol\">    
        <h3>Llista de Grups</h3>
        </div>
        <div id=\"dades\">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nom</th>
                    <th>Data Creacio</th>
                </tr>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["grups"]) ? $context["grups"] : $this->getContext($context, "grups")));
        foreach ($context['_seq'] as $context["_key"] => $context["grup"]) {
            // line 20
            echo "                <tr>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["grup"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["grup"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["grup"], "dataCreacio", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grup'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "            </table>
        </div>
    </div>
";
        
        $__internal_2533a267de5754d2c83b1ddf472ecfdb2aff4b4f562085cb6064ae377988d405->leave($__internal_2533a267de5754d2c83b1ddf472ecfdb2aff4b4f562085cb6064ae377988d405_prof);

    }

    public function getTemplateName()
    {
        return "grups/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 26,  88 => 23,  84 => 22,  80 => 21,  77 => 20,  73 => 19,  57 => 7,  51 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/grups/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block head %}*/
/*         {{ parent() }}*/
/*     {% endblock %}*/
/*     {% block body %}*/
/*         {{ parent() }}*/
/*     <div class="content">*/
/*         <div id="titol">    */
/*         <h3>Llista de Grups</h3>*/
/*         </div>*/
/*         <div id="dades">*/
/*             <table>*/
/*                 <tr>*/
/*                     <th>Id</th>*/
/*                     <th>Nom</th>*/
/*                     <th>Data Creacio</th>*/
/*                 </tr>*/
/*             {% for grup in grups %}*/
/*                 <tr>*/
/*                     <td>{{ grup.id }}</td>*/
/*                     <td>{{ grup.nom }}</td>*/
/*                     <td>{{ grup.dataCreacio.format('Y-m-d') }}</td>*/
/*                 </tr>*/
/*             {% endfor %}*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
