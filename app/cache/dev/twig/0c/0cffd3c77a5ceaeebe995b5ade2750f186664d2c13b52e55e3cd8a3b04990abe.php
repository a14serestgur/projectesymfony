<?php

/* default/content.html.twig */
class __TwigTemplate_d3de8e96de3bec4283e4c2564a0e703583c95cfda265d2ee7d7d346ac9c8cbac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81fcfad963bc4b8b7b0349d43375f0c9d290cf194ef0c6256e6034b7db48a09a = $this->env->getExtension("native_profiler");
        $__internal_81fcfad963bc4b8b7b0349d43375f0c9d290cf194ef0c6256e6034b7db48a09a->enter($__internal_81fcfad963bc4b8b7b0349d43375f0c9d290cf194ef0c6256e6034b7db48a09a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_81fcfad963bc4b8b7b0349d43375f0c9d290cf194ef0c6256e6034b7db48a09a->leave($__internal_81fcfad963bc4b8b7b0349d43375f0c9d290cf194ef0c6256e6034b7db48a09a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6a90c3140c928ee64b57532da5bcc7dbbe9d5e5e86ec34d17a6434a3f60c1a78 = $this->env->getExtension("native_profiler");
        $__internal_6a90c3140c928ee64b57532da5bcc7dbbe9d5e5e86ec34d17a6434a3f60c1a78->enter($__internal_6a90c3140c928ee64b57532da5bcc7dbbe9d5e5e86ec34d17a6434a3f60c1a78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3>Llista de Grups</h3>
    <table border=\"1\">
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Data Creacio</th>
        </tr>
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["grups"]) ? $context["grups"] : $this->getContext($context, "grups")));
        foreach ($context['_seq'] as $context["_key"] => $context["grup"]) {
            // line 12
            echo "            <tr>
                <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["grup"], "id", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["grup"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["grup"], "dataCreacio", array()), "format", array(0 => "Y-m-d"), "method"), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grup'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    </table>
";
        
        $__internal_6a90c3140c928ee64b57532da5bcc7dbbe9d5e5e86ec34d17a6434a3f60c1a78->leave($__internal_6a90c3140c928ee64b57532da5bcc7dbbe9d5e5e86ec34d17a6434a3f60c1a78_prof);

    }

    public function getTemplateName()
    {
        return "default/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 18,  64 => 15,  60 => 14,  56 => 13,  53 => 12,  49 => 11,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     <h3>Llista de Grups</h3>*/
/*     <table border="1">*/
/*         <tr>*/
/*             <th>Id</th>*/
/*             <th>Nom</th>*/
/*             <th>Data Creacio</th>*/
/*         </tr>*/
/*         {% for grup in grups %}*/
/*             <tr>*/
/*                 <td>{{ grup.id }}</td>*/
/*                 <td>{{ grup.nom }}</td>*/
/*                 <td>{{ grup.dataCreacio.format('Y-m-d') }}</td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*     </table>*/
/* {% endblock %}*/
