<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_3e49a8390c99b28d9ef9104729d077d63dccf0f4fd1fe376aa0bb54e07b0d96c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be209c6f87f593b44ceb3df6c029e4614024a1db93ad5142a7ea56bec87a9cc9 = $this->env->getExtension("native_profiler");
        $__internal_be209c6f87f593b44ceb3df6c029e4614024a1db93ad5142a7ea56bec87a9cc9->enter($__internal_be209c6f87f593b44ceb3df6c029e4614024a1db93ad5142a7ea56bec87a9cc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_be209c6f87f593b44ceb3df6c029e4614024a1db93ad5142a7ea56bec87a9cc9->leave($__internal_be209c6f87f593b44ceb3df6c029e4614024a1db93ad5142a7ea56bec87a9cc9_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c398cff763ea78f83a8d7eb653382f06e0032230d08eba23474d5757581c5b08 = $this->env->getExtension("native_profiler");
        $__internal_c398cff763ea78f83a8d7eb653382f06e0032230d08eba23474d5757581c5b08->enter($__internal_c398cff763ea78f83a8d7eb653382f06e0032230d08eba23474d5757581c5b08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c398cff763ea78f83a8d7eb653382f06e0032230d08eba23474d5757581c5b08->leave($__internal_c398cff763ea78f83a8d7eb653382f06e0032230d08eba23474d5757581c5b08_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_81f8b8c787bde51914fde8553e5bdb81f39cbd0be1f73a1acf32509a64da0b28 = $this->env->getExtension("native_profiler");
        $__internal_81f8b8c787bde51914fde8553e5bdb81f39cbd0be1f73a1acf32509a64da0b28->enter($__internal_81f8b8c787bde51914fde8553e5bdb81f39cbd0be1f73a1acf32509a64da0b28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_81f8b8c787bde51914fde8553e5bdb81f39cbd0be1f73a1acf32509a64da0b28->leave($__internal_81f8b8c787bde51914fde8553e5bdb81f39cbd0be1f73a1acf32509a64da0b28_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_00a4f0dc40048d683d915e66cf345cf0ec30d5a8e235976b0f239c1620c8be6a = $this->env->getExtension("native_profiler");
        $__internal_00a4f0dc40048d683d915e66cf345cf0ec30d5a8e235976b0f239c1620c8be6a->enter($__internal_00a4f0dc40048d683d915e66cf345cf0ec30d5a8e235976b0f239c1620c8be6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_00a4f0dc40048d683d915e66cf345cf0ec30d5a8e235976b0f239c1620c8be6a->leave($__internal_00a4f0dc40048d683d915e66cf345cf0ec30d5a8e235976b0f239c1620c8be6a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
