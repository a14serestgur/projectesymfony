<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_06cf8fa2cd8e7ad77becb5a25792003ae71069022d7c25e438d9c874b68b8648 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45356ffb6e2fde2cb9a1a665c4c03b2930991f4cd5fd7579032d4183f453e859 = $this->env->getExtension("native_profiler");
        $__internal_45356ffb6e2fde2cb9a1a665c4c03b2930991f4cd5fd7579032d4183f453e859->enter($__internal_45356ffb6e2fde2cb9a1a665c4c03b2930991f4cd5fd7579032d4183f453e859_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_45356ffb6e2fde2cb9a1a665c4c03b2930991f4cd5fd7579032d4183f453e859->leave($__internal_45356ffb6e2fde2cb9a1a665c4c03b2930991f4cd5fd7579032d4183f453e859_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_ff73dbb3f4dd0c29c74f0cfdcc49265117a105732bbeb9b3c9f43140b8bfa63d = $this->env->getExtension("native_profiler");
        $__internal_ff73dbb3f4dd0c29c74f0cfdcc49265117a105732bbeb9b3c9f43140b8bfa63d->enter($__internal_ff73dbb3f4dd0c29c74f0cfdcc49265117a105732bbeb9b3c9f43140b8bfa63d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_ff73dbb3f4dd0c29c74f0cfdcc49265117a105732bbeb9b3c9f43140b8bfa63d->leave($__internal_ff73dbb3f4dd0c29c74f0cfdcc49265117a105732bbeb9b3c9f43140b8bfa63d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_014b45d3534317019a976bc57f70a94bc0f663dc9fe7c1c718a2984995d43d01 = $this->env->getExtension("native_profiler");
        $__internal_014b45d3534317019a976bc57f70a94bc0f663dc9fe7c1c718a2984995d43d01->enter($__internal_014b45d3534317019a976bc57f70a94bc0f663dc9fe7c1c718a2984995d43d01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_014b45d3534317019a976bc57f70a94bc0f663dc9fe7c1c718a2984995d43d01->leave($__internal_014b45d3534317019a976bc57f70a94bc0f663dc9fe7c1c718a2984995d43d01_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a37dfdd64fa89cc3687a294a67380450601c16784d827e21fe299f415938e4d7 = $this->env->getExtension("native_profiler");
        $__internal_a37dfdd64fa89cc3687a294a67380450601c16784d827e21fe299f415938e4d7->enter($__internal_a37dfdd64fa89cc3687a294a67380450601c16784d827e21fe299f415938e4d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_a37dfdd64fa89cc3687a294a67380450601c16784d827e21fe299f415938e4d7->leave($__internal_a37dfdd64fa89cc3687a294a67380450601c16784d827e21fe299f415938e4d7_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
