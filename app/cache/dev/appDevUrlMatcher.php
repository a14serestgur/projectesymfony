<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectConcert
            if ($pathinfo === '/selectConcert') {
                return array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::selectConcertAction',  '_route' => 'selectConcert',);
            }

            // selectAllConcerts
            if ($pathinfo === '/selectAllConcerts') {
                return array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::selectAllConcertsAction',  '_route' => 'selectAllConcerts',);
            }

        }

        // insertConcert
        if ($pathinfo === '/insertConcert') {
            return array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::insertConcertAction',  '_route' => 'insertConcert',);
        }

        // deleteConcert
        if ($pathinfo === '/deleteConcert') {
            return array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::deleteConcertAction',  '_route' => 'deleteConcert',);
        }

        // updateConcert
        if ($pathinfo === '/updateConcert') {
            return array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::updateConcertAction',  '_route' => 'updateConcert',);
        }

        // editConcert
        if (0 === strpos($pathinfo, '/editConcert') && preg_match('#^/editConcert/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editConcert')), array (  '_controller' => 'AppBundle\\Controller\\ConcertsController::editConcertAction',));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectEntrada
            if ($pathinfo === '/selectEntrada') {
                return array (  '_controller' => 'AppBundle\\Controller\\EntradesController::selectEntradaAction',  '_route' => 'selectEntrada',);
            }

            // selectAllEntrades
            if ($pathinfo === '/selectAllEntrades') {
                return array (  '_controller' => 'AppBundle\\Controller\\EntradesController::selectAllEntradesAction',  '_route' => 'selectAllEntrades',);
            }

        }

        // insertEntrada
        if ($pathinfo === '/insertEntrada') {
            return array (  '_controller' => 'AppBundle\\Controller\\EntradesController::insertEntradaAction',  '_route' => 'insertEntrada',);
        }

        // deleteEntrada
        if ($pathinfo === '/deleteEntrada') {
            return array (  '_controller' => 'AppBundle\\Controller\\EntradesController::deleteEntradaAction',  '_route' => 'deleteEntrada',);
        }

        // updateEntrada
        if ($pathinfo === '/updateEntrada') {
            return array (  '_controller' => 'AppBundle\\Controller\\EntradesController::updateEntradaAction',  '_route' => 'updateEntrada',);
        }

        // editEntrada
        if (0 === strpos($pathinfo, '/editEntrada') && preg_match('#^/editEntrada/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editEntrada')), array (  '_controller' => 'AppBundle\\Controller\\EntradesController::editEntradaAction',));
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectEspai
            if ($pathinfo === '/selectEspai') {
                return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::selectEspaiAction',  '_route' => 'selectEspai',);
            }

            // selectAllEspais
            if ($pathinfo === '/selectAllEspais') {
                return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::selectAllEspaisAction',  '_route' => 'selectAllEspais',);
            }

        }

        // insertEspai
        if ($pathinfo === '/insertEspai') {
            return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::insertGrupAction',  '_route' => 'insertEspai',);
        }

        // deleteEspai
        if ($pathinfo === '/deleteEspai') {
            return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::deleteEspaiAction',  '_route' => 'deleteEspai',);
        }

        // selectAllEspaisOrderedByName
        if ($pathinfo === '/selectAllEspaisOrderedByName') {
            return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::selectAllEspaisOrderedByNameAction',  '_route' => 'selectAllEspaisOrderedByName',);
        }

        // updateEspai
        if ($pathinfo === '/updateEspai') {
            return array (  '_controller' => 'AppBundle\\Controller\\EspaisController::updateGrupAction',  '_route' => 'updateEspai',);
        }

        // editEspai
        if (0 === strpos($pathinfo, '/editEspai') && preg_match('#^/editEspai/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editEspai')), array (  '_controller' => 'AppBundle\\Controller\\EspaisController::editEspaiAction',));
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectGrup
            if ($pathinfo === '/selectGrup') {
                return array (  '_controller' => 'AppBundle\\Controller\\GrupController::selectGrupAction',  '_route' => 'selectGrup',);
            }

            // selectAllGrups
            if ($pathinfo === '/selectAllGrups') {
                return array (  '_controller' => 'AppBundle\\Controller\\GrupController::selectAllGrupsAction',  '_route' => 'selectAllGrups',);
            }

        }

        // insertGrup
        if ($pathinfo === '/insertGrup') {
            return array (  '_controller' => 'AppBundle\\Controller\\GrupController::insertGrupAction',  '_route' => 'insertGrup',);
        }

        // deleteGrup
        if ($pathinfo === '/deleteGrup') {
            return array (  '_controller' => 'AppBundle\\Controller\\GrupController::deleteGrupAction',  '_route' => 'deleteGrup',);
        }

        // selectAllGrupsOrderedByName
        if ($pathinfo === '/selectAllGrupsOrderedByName') {
            return array (  '_controller' => 'AppBundle\\Controller\\GrupController::selectAllGrupsOrderedByNameAction',  '_route' => 'selectAllGrupsOrderedByName',);
        }

        // updateGrup
        if ($pathinfo === '/updateGrup') {
            return array (  '_controller' => 'AppBundle\\Controller\\GrupController::updateGrupAction',  '_route' => 'updateGrup',);
        }

        // editGrup
        if (0 === strpos($pathinfo, '/editGrup') && preg_match('#^/editGrup/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editGrup')), array (  '_controller' => 'AppBundle\\Controller\\GrupController::editGrupAction',));
        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
