<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Espais;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EspaisController extends Controller
{
    /*Buscar Espai per nom*/
    
    /**
     * @Route("/selectEspai", name="selectEspai")
     */
    public function selectEspaiAction(Request $request)
    {
        $espai = new Espais();
        
        $form = $this->createFormBuilder($espai)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'select'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $espais = $this->getDoctrine()
                ->getRepository('AppBundle:Espais')
                ->findByNom($espai->getNom());
            if(count($espais)==0){
                return $this->render('default/missatge.html.twig', array('missatge' => 'No hi ha Espais'));
            }
            return $this->render('espais/content.html.twig', array('espais' => $espais));
        }
        return $this->render('default/form.html.twig', array('title' => 'Select Espai', 'form' => $form->createView(),));
    }
    
    /*Buscar tots els Espais*/
    
    /**
     * @Route("/selectAllEspais", name="selectAllEspais")
     */
    public function selectAllEspaisAction()
    {
        $espais = $this->getDoctrine()
            ->getRepository('AppBundle:Espais')
            ->findAll();

        if (count($espais)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Espais'));
        }
        return $this->render('espais/content.html.twig', array(
            'espais' => $espais));
    }
    
    /*Nou Espai*/
    
    /**
     * @Route("/insertEspai", name="insertEspai")
     */
    public function insertGrupAction(Request $request)
    {
        $espai = new Espais();
        
        $form = $this->createFormBuilder($espai)
            ->add('nom', TextType::class)
            ->add('localitzacio', TextType::class)
            ->add('placesMaximes', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($espai);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Espai insertat: '. $espai->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insertar Espai',
            'form' => $form->createView(),
            ));
    }
    
    /*Elimina Espai*/
    
    /**
     * @Route("/deleteEspai", name="deleteEspai")
     */
    public function deleteEspaiAction(Request $request)
    {
        $espai = new Espais();

        $form = $this->createFormBuilder($espai)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $espai = $em->getRepository('AppBundle:Espais')
                      ->findOneByNom($espai->getNom());
            if (!$espai) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Espais amb aquest Nom'));
            }
            $id = $espai->getId();
            $em->remove($espai);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Espai Eliminat: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Eliminar Espai',
            'form' => $form->createView(),
        ));
    }
    
    /*Buscar Espais Ordenats per nom*/
    
    /**
     * @Route("/selectAllEspaisOrderedByName", name="selectAllEspaisOrderedByName")
     */
    public function selectAllEspaisOrderedByNameAction()
    {
        $espais = $this->getDoctrine()
            ->getRepository('AppBundle:Espais')
            ->findAllOrderedByNom();

        if (count($espais)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Espais'));
        }
        return $this->render('espais/content.html.twig', array(
            'espais' => $espais));
    }
    
    /*Formulari editar Espai*/
    
    /**
     * @Route("/updateEspai", name="updateEspai")
     */
    public function updateGrupAction(Request $request)
    {
        $espai = new Espais();

        $form = $this->createFormBuilder($espai)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $espai = $em->getRepository('AppBundle:Espais')
                ->findOneByNom($espai->getNom());
            if (!$espai) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Espais amb aquest nom.'));
            }
            return $this->redirectToRoute('editEspai',array('nom' => $espai->getNom()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Espai',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editEspai/{nom}", name="editEspai")
     */
    public function editEspaiAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $espai = $em->getRepository('AppBundle:Espais')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($espai)
            ->add('nom', TextType::class)
            ->add('localitzacio', TextType::class)
            ->add('placesMaximes', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Espai updated id: '. $espai->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Espai',
            'form' => $form->createView(),
        ));
    }
}


















