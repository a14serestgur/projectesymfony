<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Grups;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class GrupController extends Controller
{
    /*Buscar Grup per nom*/
    
    /**
     * @Route("/selectGrup", name="selectGrup")
     */
     public function selectGrupAction(Request $request)
     {
         $grup = new Grups();
         
         $form = $this->createFormBuilder($grup)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'select'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $grups = $this->getDoctrine()
                ->getRepository('AppBundle:Grups')
                ->findByNom($grup->getNom());
            if(count($grups)==0) {
                return $this->render('default/missatge.html.twig', array('missatge' => 'No hi ha Grups'));
            }
            return $this->render('grups/content.html.twig', array(
                'grups' => $grups)); 
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Grup',
            'form' => $form->createView(),
        ));
    }
    
    /*Buscar tots els Grups*/
    
    /**
     * @Route("/selectAllGrups", name="selectAllGrups")
     */
    public function selectAllGrupsAction()
    {
        $grups = $this->getDoctrine()
            ->getRepository('AppBundle:Grups')
            ->findAll();

        if (count($grups)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Grups'));
        }
        return $this->render('grups/content.html.twig', array(
            'grups' => $grups));
    }
    
    /*Nou Grup*/
    
    /**
     * @Route("/insertGrup", name="insertGrup")
     */
    public function insertGrupAction(Request $request)
    {
        $grup = new Grups();
        
        $form = $this->createFormBuilder($grup)
            ->add('nom', TextType::class)
            ->add('dataCreacio', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($grup);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Grup insertat: '. $grup->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insertar Grup',
            'form' => $form->createView(),
            ));
    }
    
    /*Elimina Grup*/
    
    /**
     * @Route("/deleteGrup", name="deleteGrup")
     */
    public function deleteGrupAction(Request $request)
    {
        $grup = new Grups();

        $form = $this->createFormBuilder($grup)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $grup = $em->getRepository('AppBundle:Grups')
                      ->findOneByNom($grup->getNom());
            if (!$grup) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha grups amb aquest Nom'));
            }
            $id = $grup->getId();
            $em->remove($grup);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Grup Eliminat: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Eliminar Grup',
            'form' => $form->createView(),
        ));
    }
    
    /*Buscar Grups Ordenats per nom*/
    
    /**
     * @Route("/selectAllGrupsOrderedByName", name="selectAllGrupsOrderedByName")
     */
    public function selectAllGrupsOrderedByNameAction()
    {
        $grups = $this->getDoctrine()
            ->getRepository('AppBundle:Grups')
            ->findAllOrderedByNom();

        if (count($grups)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Grups'));
        }
        return $this->render('grups/content.html.twig', array(
            'grups' => $grups));
    }
    
    /*Formulari editar Grup*/
    
    /**
     * @Route("/updateGrup", name="updateGrup")
     */
    public function updateGrupAction(Request $request)
    {
        $grup = new Grups();

        $form = $this->createFormBuilder($grup)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $grup = $em->getRepository('AppBundle:Grups')
                ->findOneByNom($grup->getNom());
            if (!$grup) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Grups amb aquest nom.'));
            }
            return $this->redirectToRoute('editGrup',array('nom' => $grup->getNom()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Grup',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editGrup/{nom}", name="editGrup")
     */
    public function editGrupAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $grup = $em->getRepository('AppBundle:Grups')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($grup)
            ->add('nom', TextType::class)
            ->add('dataCreacio', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Grup updated id: '. $grup->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Grup',
            'form' => $form->createView(),
        ));
    }
}
