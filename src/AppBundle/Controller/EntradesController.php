<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entrades;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EntradesController extends Controller
{
    /*Buscar Entrada per nom*/
    
    /**
     * @Route("/selectEntrada", name="selectEntrada")
     */
    public function selectEntradaAction(Request $request)
    {
        $entrada = new Entrades();
        
        $form = $this->createFormBuilder($entrada)
            ->add('nom', EntityType::class, array('class' => 'AppBundle:Entrades', 'choice_label' => 'nom'))
            ->add('save', SubmitType::class, array('label' => 'select'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $entrades = $this->getDoctrine()
                ->getRepository('AppBundle:Entrades')
                ->findByData($entrada->getData());
            if(count($entrada)==0){
                return $this->render('default/missatge.html.twig', array('missatge' => 'No hi ha Entrades'));
            }
            return $this->render('entrades/content.html.twig', array('entrades' => $entrades));
        }
        return $this->render('default/form.html.twig', array('title' => 'Select Entrada', 'form' => $form->createView(),));
    }
    
    /*Buscar totes les Entrades*/
    
    /**
     * @Route("/selectAllEntrades", name="selectAllEntrades")
     */
    public function selectAllEntradesAction()
    {
        $entrades = $this->getDoctrine()
            ->getRepository('AppBundle:Entrades')
            ->findAll();

        if (count($entrades)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Entrades'));
        }
        return $this->render('entrades/content.html.twig', array(
            'entrades' => $entrades));
    }
    
    /*Nova Entrada*/
    
    /**
     * @Route("/insertEntrada", name="insertEntrada")
     */
    public function insertEntradaAction(Request $request)
    {
        $entrada = new Entrades();
        
        $form = $this->createFormBuilder($entrada)
            ->add('idConcert', EntityType::class, array('class' => 'AppBundle:Concerts','choice_label' => 'nom' ))
            ->add('preu', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $nom = $entrada->getPreu().'€'.$entrada->getIdConcert()->getNom();
            $entrada->setNom($nom);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entrada);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Entrada insertada: '. $entrada->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insertar Entrada',
            'form' => $form->createView(),
            ));
    }
    
    /*Elimina Entrada*/
    
    /**
     * @Route("/deleteEntrada", name="deleteEntrada")
     */
    public function deleteEntradaAction(Request $request)
    {
        $entrada = new Entrades();

        $form = $this->createFormBuilder($entrada)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entrada = $em->getRepository('AppBundle:Entrades')
                      ->findOneByNom($entrada->getNom());
            if (!$entrada) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Entrades amb aquest nom'));
            }
            $nom = $entrada->getNom();
            $em->remove($entrada);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Entrada Eliminada: '.$nom
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Eliminar Entrada',
            'form' => $form->createView(),
        ));
    }
    
    /*Formulari editar Entrada*/
    
    /**
     * @Route("/updateEntrada", name="updateEntrada")
     */
    public function updateEntradaAction(Request $request)
    {
        $entrada = new Entrades();

        $form = $this->createFormBuilder($entrada)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entrada = $em->getRepository('AppBundle:Entrades')
                ->findOneByNom($entrada->getNom());
            if (!$entrada) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Entrades amb aquest nom '));
            }
            return $this->redirectToRoute('editEntrada',array('nom' => $entrada->getNom()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Entrada',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editEntrada/{nom}", name="editEntrada")
     */
    public function editEntradaAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entrada = $em->getRepository('AppBundle:Entrades')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($entrada)
            ->add('idConcert', EntityType::class, array('class' => 'AppBundle:Concerts','choice_label' => 'nom' ))
            ->add('preu', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nom = $entrada->getPreu().'€'.$entrada->getIdConcert()->getNom();
            $entrada->setNom($nom);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Entrada updated id: '. $entrada->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Entrada',
            'form' => $form->createView(),
        ));
    }
}
?>