<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concerts;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ConcertsController extends Controller
{
    /*Buscar Concert per data*/
    
    /**
     * @Route("/selectConcert", name="selectConcert")
     */
    public function selectConcertAction(Request $request)
    {
        $concert = new Concerts();
        
        $form = $this->createFormBuilder($concert)
            ->add('data', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'select'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $concerts = $this->getDoctrine()
                ->getRepository('AppBundle:Concerts')
                ->findByData($concert->getData());
            if(count($concerts)==0){
                return $this->render('default/missatge.html.twig', array('missatge' => 'No hi ha Concerts'));
            }
            return $this->render('concerts/content.html.twig', array('concerts' => $concerts));
        }
        return $this->render('default/form.html.twig', array('title' => 'Select Concert', 'form' => $form->createView(),));
    }
    
    /*Buscar tots els Concerts*/
    
    /**
     * @Route("/selectAllConcerts", name="selectAllConcerts")
     */
    public function selectAllConcertsAction()
    {
        $concerts = $this->getDoctrine()
            ->getRepository('AppBundle:Concerts')
            ->findAll();

        if (count($concerts)==0) {
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'No hi ha Concerts'));
        }
        return $this->render('concerts/content.html.twig', array(
            'concerts' => $concerts));
    }
    
    /*Nou Concert*/
    
    /**
     * @Route("/insertConcert", name="insertConcert")
     */
    public function insertConcertAction(Request $request)
    {
        $concert = new Concerts();
        
        $form = $this->createFormBuilder($concert)
            ->add('data', DateType::class)
            ->add('idgrup', EntityType::class, array('class' => 'AppBundle:Grups','choice_label' => 'nom' ))
            ->add('idEspai', EntityType::class, array('class' => 'AppBundle:Espais','choice_label' => 'nom' ))
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $nom = $concert->getData()->format('Y-m-d').$concert->getIdgrup()->getNom();
            $concert->setNom($nom);
            $em = $this->getDoctrine()->getManager();
            $em->persist($concert);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Concert insertat: '. $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insertar Concert',
            'form' => $form->createView(),
            ));
    }
    
    /*Elimina Concert*/
    
    /**
     * @Route("/deleteConcert", name="deleteConcert")
     */
    public function deleteConcertAction(Request $request)
    {
        $concert = new Concerts();

        $form = $this->createFormBuilder($concert)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $concert = $em->getRepository('AppBundle:Concerts')
                      ->findOneByNom($concert->getNom());
            if (!$concert) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Concerts amb aquest nom '.$concert->getNom()));
            }
            $nom = $concert->getNom();
            $em->remove($concert);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Concert Eliminat: '.$nom
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Eliminar Concert',
            'form' => $form->createView(),
        ));
    }
    
    /*Formulari editar Concert*/
    
    /**
     * @Route("/updateConcert", name="updateConcert")
     */
    public function updateConcertAction(Request $request)
    {
        $concert = new Concerts();

        $form = $this->createFormBuilder($concert)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $concert = $em->getRepository('AppBundle:Concerts')
                ->findOneByNom($concert->getNom());
            if (!$concert) {
                return $this->render('default/missatge.html.twig', array(
                    'missatge' => 'No hi ha Concerts amb aquest nom '));
            }
            return $this->redirectToRoute('editConcert',array('nom' => $concert->getNom()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Concert',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editConcert/{nom}", name="editConcert")
     */
    public function editConcertAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $concert = $em->getRepository('AppBundle:Concerts')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($concert)
            ->add('data', DateType::class)
            ->add('idgrup', EntityType::class, array('class' => 'AppBundle:Grups','choice_label' => 'nom' ))
            ->add('idEspai', EntityType::class, array('class' => 'AppBundle:Espais','choice_label' => 'nom' ))
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nom = $concert->getData()->format('Y-m-d').$concert->getIdgrup()->getNom();
            $concert->setNom($nom);
            $em->flush();
            return $this->render('default/missatge.html.twig', array(
                'missatge' => 'Concert updated id: '. $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Concert',
            'form' => $form->createView(),
        ));
    }
}