<?php
// src/AppBundle/Entity/Entrades.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Entrades")
 */
 class Entrades
 {
     
    /*-----Propietats de l'objecte-----*/
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     */
     protected $nom;
    
    /**
     * @ORM\Column(type="integer")
     */ 
    /** 
     * @ORM\ManyToOne(targetEntity="Concerts", inversedBy="id")
     * @ORM\JoinColumn(name="idConcert", referencedColumnName="id")
     */
    
    protected $idConcert;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $preu;
    
    /*---------------------------------*/
    
    /*-----Funcions-----*/
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set nom
     *
     * @param String $nom
     * @return mom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
    
    /**
     * Set idConcert
     * 
     * @return integer
     */
    public function setIdConcert($idConcert)
    {
        $this->idConcert = $idConcert;
        
        return $this;
    }
    
    /**
     * Get idConcert
     *
     * @return integer 
     */
    public function getIdConcert()
    {
        return $this->idConcert;
    }
    
    /**
     * Set preu
     * 
     * @return decimal
     */
    public function setPreu($preu)
    {
        $this->preu = $preu;
        
        return $this;
    }
    
    /**
     * Get preu
     *
     * @return decimal 
     */
    public function getPreu()
    {
        return $this->preu;
    }
    
    /**
     * toString
     */
    public function __toString()
    {
        return "(".$this->id.", ".$this->preu.", ".$this->idConcert.")";
    }
}
