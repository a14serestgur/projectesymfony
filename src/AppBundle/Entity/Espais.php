<?php
// src/AppBundle/Entity/Espais.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Espais")
 */
 
 /**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\EspaistRepository")
 */
class Espais
{
    
    /*-----Propietats de l'objecte-----*/
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nom;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $localitzacio;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $placesMaximes;
    
    /*---------------------------------*/
    
    /*-----Funcions-----*/
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set nom
     *
     * @param string $nom
     * @return Espai
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
    
    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set localitzacio
     * 
     * @return string
     */
    public function setLocalitzacio($localitzacio)
    {
        $this->localitzacio = $localitzacio;
         
        return $this;
    }
     
    /**
     * Get localitzacio
     * 
     * @return string
     */
    public function getLocalitzacio()
    {
        return $this->localitzacio;
    }
      
    /**
     * Set placesMaximes
     * 
     * @return integer
     */
    public function setPlacesMaximes($placesMaximes)
    {
        $this->placesMaximes = $placesMaximes;
        
        return $this;
    }
    
    /**
     * Get placesMaximes
     * 
     * @return integer
     */
    public function getPlacesMaximes()
    {
        return $this->placesMaximes;
    }
    
    /**
     * toString
     */
    public function __toString()
    {
        return "(".$this->id.", ".$this->nom.", ".$this->localitzacio.", ".$this.placesMaximes.")";
    }
      
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add id
     *
     * @param \AppBundle\Entity\Concerts $id
     * @return Espais
     */
    public function addId(\AppBundle\Entity\Concerts $id)
    {
        $this->id[] = $id;

        return $this;
    }

    /**
     * Remove id
     *
     * @param \AppBundle\Entity\Concerts $id
     */
    public function removeId(\AppBundle\Entity\Concerts $id)
    {
        $this->id->removeElement($id);
    }
    
    /**
     * @ORM\OneToMany(targetEntity="Concerts", mappedBy="Espais")
     */
}
