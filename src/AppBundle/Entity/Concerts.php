<?php
// src/AppBundle/Entity/Entrades.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Concerts")
 */
 class Concerts
 {
     
    /*-----Propietats de l'objecte-----*/
     
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string")
     */
     protected $nom;
     
    /**
     * @ORM\Column(type="integer")
     */
    /** 
     * @ORM\ManyToOne(targetEntity="Grups", inversedBy="id")
     * @ORM\JoinColumn(name="idGrup", referencedColumnName="id")

     */
    protected $idGrup;
     
    /**
     * @ORM\Column(type="datetime")
     */
    protected $data;
      
    /**
     * @ORM\Column(type="integer")
     */
    /**
     * @ORM\ManyToOne(targetEntity="Espais", inversedBy="id")
     * @ORM\JoinColumn(name="idEspai", referencedColumnName="id")
     */
    protected $idEspai;
       
        
    /*---------------------------------*/
    
    /*-----Funcions-----*/
        
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set nom
     *
     * @param String $nom
     * @return mom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
        
    /**
     * Set idGrup
     *
     * @param integer $idGrup
     * @return idGrup
     */
    public function setIdGrup($idGrup)
    {
        $this->idGrup = $idGrup;

        return $this;
    }
        
    /**
     * Get idGrup
     * 
     * @return Integer 
     */
   public function getIdGrup()
    {
        return $this->idGrup;
    }
        
    /**
     * Set data
     *
     * @param DATE $data
     * @return data
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
        
    /**
     * Get data
     * 
     * @return DATE 
     */
    public function getData()
    {
        return $this->data;
    }
        
    /**
     * Set idEspai
     *
     * @param integer $idEspai
     * @return idEspai
     */
    public function setIdEspai($idEspai)
    {
        $this->idEspai = $idEspai;

        return $this;
    }
        
    /**
     * Get idEspai
     * 
     * @return integer 
     */
    public function getIdEspai()
    {
        return $this->idEspai;
    }
    
    /**
     * toString
     */
    public function __toString()
    {
        return "(".$this->id.", ".$this->nom.", ".$this->idGrup.", ".$this->data.", ".$this->idEspai.")";
    }
    
    /**
     * @ORM\OneToMany(targetEntity="Entrades", mappedBy="Concerts")
     */
     
    
}
