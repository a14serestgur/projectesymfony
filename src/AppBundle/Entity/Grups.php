<?php
// src/AppBundle/Entity/Grups.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Grups")
 */
 
 /**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GrupstRepository")
 */
 class Grups
 {
    
    /*-----Propietats de l'objecte-----*/

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
     
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nom;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataCreacio;
    
    /*---------------------------------*/
    
    /*-----Funcions-----*/
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set nom
     *
     * @param string $nom
     * @return Grup
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Get data
     * 
     * @return date
     */
    public function getDataCreacio()
    {
        return $this->dataCreacio;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setDataValue()
    {
        $this->dataCreacio = new \dateTime();
    }
    
    /**
     * toString
     */
    public function __toString(){
        return "(".$this->id.", ".$this->nom.", ".$this->dataCreacio->format('Y-m-d').")";
    }
    
    /*------------------*/
 
    /**
     * Set data
     *
     * @param \DateTime $data
     * @return Grups
     */
    public function setDataCreacio($dataCreacio)
    {
        $this->dataCreacio = $dataCreacio;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add id
     *
     * @param \AppBundle\Entity\Concerts $id
     * @return Grups
     */
    public function addId(\AppBundle\Entity\Concerts $id)
    {
        $this->id[] = $id;

        return $this;
    }

    /**
     * Remove id
     *
     * @param \AppBundle\Entity\Concerts $id
     */
    public function removeId(\AppBundle\Entity\Concerts $id)
    {
        $this->id->removeElement($id);
    }
    /** 
     * @ORM\OneToMany(targetEntity="Concerts", mappedBy="Grups")
     */
}
